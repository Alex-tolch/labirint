from multiprocessing import connection
import os
import sqlite3
from sqlite3 import Error
import shutil
import hashlib

class Database:

    def __init__(self):
        table_name = "Labirint"
        create_table = f""" CREATE TABLE {table_name} (
                Username CHAR(25) NOT NULL,
                Password CHAR(25) NOT NULL,
                Role CHAR(25) NOT NULL
            ); """
        if not os.path.exists("./database"):
            os.mkdir("./database")
        self.connection = sqlite3.connect("./database/sqlite.db")
        self.cursor = self.connection.cursor()
        try:
            self.create_table(create_table)
            self.add_user('admin', 'admin', 'admin')
        except Exception as e:
            pass
            # print(f"Table {table_name}, sqlite")

    def create_table(self, table):
        self.cursor.execute(table)
        print("Table created")

    def get_table(self, table):
        self.cursor.execute(f"SELECT * FROM {table};")
        
        return self.cursor.fetchall()

    def add_user(self, username, password, role):
        password = self.pass_to_hash(password)
        sqlite_insert_query = f"INSERT INTO Labirint\
                          (Username, Password, Role)\
                           VALUES\
                          ('{username}','{password}','{role}')"
        self.cursor.execute(sqlite_insert_query)
        connection = self.connection.commit()
        
    
    def check_user(self, username, password):
        sqlite_insert_query = f"SELECT EXISTS(SELECT 1 FROM Labirint WHERE Username='{username}');"
        self.cursor.execute(sqlite_insert_query)
        
        if int(self.cursor.fetchall()[0][0]) == 1:
            return True
        else:
            return False
    
    def check_correct_login(self, username, password):
        password = self.pass_to_hash(password)
        sqlite_insert_query = f"SELECT EXISTS(SELECT 1 FROM Labirint WHERE Username='{username}' AND Password='{password}');"
        self.cursor.execute(sqlite_insert_query)
        
        if int(self.cursor.fetchall()[0][0]) == 1:
            return True
        else:
            return False

    def pass_to_hash(self, password):
        hash_object = hashlib.sha256(bytes(password, encoding='utf8'))
        hex_dig = hash_object.hexdigest()
        return hex_dig


    def get_user_role(self, username, password):
        password = self.pass_to_hash(password)
        sqlite_insert_query = f"SELECT * FROM Labirint WHERE Username='{username}' AND Password='{password}';"
        self.cursor.execute(sqlite_insert_query)
        
        return self.cursor.fetchone()[-1]
    # def initial_table(self, create_table, table_name):




    # def __init__(self, **kwargs):
    #     self.filename = kwargs.get('filename')
    #     self.table = kwargs.get('table', 'test')

    # def sql_do(self, sql, *params):
    #     self._db.execute(sql, params)
    #     self._db.commit()

    # def insert(self, row):
    #     self._db.execute('insert into {} (t1, i1) values (?, ?)'.format(self._table), (row['t1'], row['i1']))
    #     self._db.commit()

    # def retrieve(self, key):
    #     cursor = self._db.execute('select * from {} where t1 = ?'.format(self._table), (key,))
    #     return dict(cursor.fetchone())

    # def update(self, row):
    #     self._db.execute(
    #         'update {} set i1 = ? where t1 = ?'.format(self._table),
    #         (row['i1'], row['t1']))
    #     self._db.commit()

    # def delete(self, key):
    #     self._db.execute('delete from {} where t1 = ?'.format(self._table), (key,))
    #     self._db.commit()

    # def disp_rows(self):
    #     cursor = self._db.execute('select * from {} order by t1'.format(self._table))
    #     for row in cursor:
    #         print('  {}: {}'.format(row['t1'], row['i1']))

    # def __iter__(self):
    #     cursor = self._db.execute('select * from {} order by t1'.format(self._table))
    #     for row in cursor:
    #         yield dict(row)

    # @property
    # def filename(self): return self._filename
    # @filename.setter
    # def filename(self, fn):
    #     self._filename = fn
    #     self._db = sqlite3.connect(fn)
    #     self._db.row_factory = sqlite3.Row
    # @filename.deleter
    # def filename(self): self.close()
    # @property
    # def table(self): return self._table
    # @table.setter
    # def table(self, t): self._table = t
    # @table.deleter
    # def table(self): self._table = 'test'

    # def close(self):
    #         self._db.close()
    #         del self._filename
    
    # def __init__(self, path, filename):
    #     conn = None
    #     try:
    #         if not os.path.exists(path):
    #             os.makedirs(path)
    #     except Error as e:
    #         print(e)
    #     finally:
    #         conn = sqlite3.connect(os.path.join(path, filename))
    #         print(f'database created: sqlite version {sqlite3.version}')
    #         conn.close()
    #     return conn
    
#     def create_user(self, login, password, role):
#         print()
