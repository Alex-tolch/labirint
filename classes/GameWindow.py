import pygame
import time

BLOCK = 80
class Game:
	def __init__(self, maze, theme):
		pygame.init()
		self.WIDTH = len(maze)*BLOCK
		self.HEIGHT = len(maze[0])*BLOCK
		self.screen = pygame.display.set_mode((self.WIDTH, self.HEIGHT))
		pygame.display.set_caption("Лабиринт")
		self.clock = pygame.time.Clock()
		self.status = False
		self.maze = maze
		self.maze_h = len(maze)
		self.maze_w = len(maze[0])
		self.WIDTH = self.maze_w *BLOCK
		self.HEIGHT = self.maze_h*BLOCK
		self.gamer_pos = self.get_enter()
		self.icon = pygame.transform.scale(pygame.image.load('./images/serik.png'), (BLOCK, BLOCK))
		self.theme = theme

	def print_maze(self):
		self.screen.fill(pygame.Color('black'))
		for i in range(0, self.maze_h):
			for j in range(0, self.maze_w):
				if (self.maze[i][j] == 0):
					pygame.draw.rect(self.screen, self.theme[0], pygame.Rect((i*BLOCK), (j*BLOCK), BLOCK, BLOCK))
				elif (self.maze[i][j] == 3):
					pygame.draw.rect(self.screen, self.theme[2], pygame.Rect((i*BLOCK), (j*BLOCK), BLOCK, BLOCK))
				elif (self.maze[i][j] == 5):
					pygame.draw.rect(self.screen, self.theme[3], pygame.Rect((i*BLOCK), (j*BLOCK), BLOCK, BLOCK))
				elif (self.maze[i][j] == 1):
					pygame.draw.rect(self.screen, self.theme[1], pygame.Rect((i*BLOCK), (j*BLOCK), BLOCK, BLOCK))
		self.screen.blit(self.icon, self.gamer_pos)

	def get_enter(self):
		for i in range(0, self.maze_h):
			for j in range(0, self.maze_w):
				if self.maze[i][j] == 3:
					return ((i*BLOCK), (j*BLOCK))
	
	def get_pos_in_maze(self, x, y):
		x = (x)/BLOCK
		y = (y)/BLOCK
		return int(x), int(y)
	
	def update_position(self, x, y):
		new_cell = self.get_pos_in_maze(self.gamer_pos[0]+(BLOCK*x), self.gamer_pos[1]+(BLOCK*y))
		try:
			if self.maze[new_cell[0]][new_cell[1]] == 1 or self.maze[new_cell[0]][new_cell[1]] == 3:
				self.gamer_pos = self.gamer_pos[0]+(BLOCK*x), self.gamer_pos[1]+(BLOCK*y)
			elif self.maze[new_cell[0]][new_cell[1]] == 5:
				self.gamer_pos = self.gamer_pos[0]+(BLOCK*x), self.gamer_pos[1]+(BLOCK*y)
				
				self.icon = pygame.transform.scale(pygame.image.load('./images/leha.png'), (BLOCK, BLOCK))
				self.print_maze()
				self.status = True
		except IndexError:
			self.gamer_pos = self.gamer_pos

	def manual_game(self):
		run = True
		while run:
			self.screen.fill(pygame.Color('black'))
			for event in pygame.event.get():
				if event.type == pygame.KEYDOWN:
					if event.key == pygame.K_LEFT or event.key == pygame.K_a:
						self.update_position(-1, 0)
					if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
						self.update_position(1, 0)
					if event.key == pygame.K_UP or event.key == pygame.K_w:
						self.update_position(0, -1)
					if event.key == pygame.K_DOWN or event.key == pygame.K_s:
						self.update_position(0, 1)
				if event.type == pygame.QUIT or self.status:
					run = False
			self.print_maze()
			pygame.display.flip()
			self.clock.tick(30)
		pygame.quit()
		return self.status

	def bfs_game(self):
		start = 0, 0
		end = 0, 0
		for i in range(0, self.maze_h):
			for j in range(0, self.maze_w):
				if self.maze[i][j] == 3:
					start = i, j
				elif self.maze[i][j] == 5:
					end = i, j
		m = []
		for i in range(self.maze_h):
			m.append([])
			for j in range(self.maze_w):
				m[-1].append(0)
		self.maze[end[0]][end[1]] = 1
		i,j = start
		m[i][j] = 1
		self.print_maze()
		self.gamer_pos = i*BLOCK, j*BLOCK
		self.screen.blit(self.icon, self.gamer_pos)
		def make_step(k):
			for i in range(self.maze_h):
				for j in range(self.maze_w):
					if m[i][j] == k:
						if i>0 and m[i-1][j] == 0 and self.maze[i-1][j] == 1:
							m[i-1][j] = k + 1
						if j>0 and m[i][j-1] == 0 and self.maze[i][j-1] == 1:
							m[i][j-1] = k + 1
						if i<len(m)-1 and m[i+1][j] == 0 and self.maze[i+1][j] == 1:
							m[i+1][j] = k + 1
						if j<len(m[i])-1 and m[i][j+1] == 0 and self.maze[i][j+1] == 1:
							m[i][j+1] = k + 1
							
						# self.screen.blit(self.icon, self.gamer_pos)
						if k > 1:
							pygame.draw.rect(self.screen, self.theme[2], pygame.Rect((i*BLOCK), (j*BLOCK), BLOCK, BLOCK))
						pygame.display.flip()
						self.clock.tick(30)

		k = 0
		while m[end[0]][end[1]] == 0:
			k += 1
			make_step(k)
		pygame.draw.rect(self.screen, self.theme[3], pygame.Rect((end[0]*BLOCK), (end[1]*BLOCK), BLOCK, BLOCK))
		pygame.display.flip()
		# ищем самый короткий путь
		i, j = end
		k = m[i][j]
		the_path = [(i,j)]
		while k > 1:
			if i > 0 and m[i - 1][j] == k-1:
				i, j = i-1, j
				the_path.append((i, j))
				k-=1
			elif j > 0 and m[i][j - 1] == k-1:
				i, j = i, j-1
				the_path.append((i, j))
				k-=1
			elif i < len(m) - 1 and m[i + 1][j] == k-1:
				i, j = i+1, j
				the_path.append((i, j))
				k-=1
			elif j < len(m[i]) - 1 and m[i][j + 1] == k-1:
				i, j = i, j+1
				the_path.append((i, j))
				k -= 1
		
		for i in the_path:
			if i != start:
				pygame.draw.rect(self.screen, self.theme[3], pygame.Rect((i[0]*BLOCK), (i[1]*BLOCK), BLOCK, BLOCK))
				pygame.display.flip()
				self.clock.tick(30)

		for i in reversed(the_path):
			if i != start:
				self.gamer_pos = i[0]*BLOCK, i[1]*BLOCK
				self.screen.blit(self.icon, self.gamer_pos)
				pygame.display.flip()
				self.clock.tick(5)
				# pygame.draw.rect(self.screen, self.theme[3], pygame.Rect(((i[0]-1)*BLOCK), ((i-1)[1]*BLOCK), BLOCK, BLOCK))
				# pygame.display.flip()
		# run = True
		# while run:
		# 	self.screen.fill(pygame.Color('black'))
		# 	for event in pygame.event.get():
		# 		if event.type == pygame.QUIT or self.status:
		# 			run = False
			
		# 	pygame.display.flip()
		# 	self.clock.tick(30)
		# pygame.quit()
		# return self.status

# maze = [[0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0], [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0], [0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0], [0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0], [0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0], [0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0], [0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0], [0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0], [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0], [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0], [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0]]
# # maze = [[0, 0, 0, 0, 0, 0, 0], [3, 1, 1, 1, 1, 1, 0], [0, 1, 0, 0, 0, 0, 0], [0, 1, 0, 1, 1, 1, 0], [0, 1, 0, 1, 0, 1, 0], [0, 1, 1, 1, 0, 1, 5], [0, 0, 0, 0, 0, 0, 0]]
# theme = ['#0086e6', '#adddff', '#ff4538', '#0fff4b']
# game = Game(maze, theme)
# # # game.manual_game()

# game.bfs_game()