# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui_templates\AdminMenu_2.ui'
#
# Created by: PyQt5 UI code generator 5.15.7
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.



from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView, QGraphicsRectItem, QMessageBox, QFileDialog
from PyQt5.QtGui import QBrush, QPen, QColor
from PyQt5.QtCore import Qt
import os
from datetime import datetime
import json, re
import numpy

from classes.labirint3 import PrimsMaze
from classes.labirint2 import Backtracking


class Ui_AdminMenu(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        # MainWindow.resize(723, 462)
        MainWindow.setFixedSize(723, 462) ###
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox_3 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 10, 241, 251))
        self.groupBox_3.setObjectName("groupBox_3")
        self.pushButton_2 = QtWidgets.QPushButton(self.groupBox_3)
        self.pushButton_2.setGeometry(QtCore.QRect(40, 210, 131, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        self.groupBox_2 = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 90, 211, 81))
        self.groupBox_2.setObjectName("groupBox_2")
        self.spinBox = QtWidgets.QSpinBox(self.groupBox_2)
        self.spinBox.setGeometry(QtCore.QRect(110, 20, 42, 22))
        self.spinBox.setMinimum(7)
        self.spinBox.setMaximum(21)
        self.spinBox.setSingleStep(2)
        self.spinBox.setObjectName("spinBox")
        self.spinBox_2 = QtWidgets.QSpinBox(self.groupBox_2)
        self.spinBox_2.setGeometry(QtCore.QRect(110, 50, 42, 22))
        self.spinBox_2.setMinimum(7)
        self.spinBox_2.setMaximum(21)
        self.spinBox_2.setSingleStep(2)
        self.spinBox_2.setObjectName("spinBox_2")
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setGeometry(QtCore.QRect(10, 20, 101, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.groupBox_2)
        self.label_2.setGeometry(QtCore.QRect(10, 50, 101, 16))
        self.label_2.setObjectName("label_2")
        self.pushButton = QtWidgets.QPushButton(self.groupBox_3)
        self.pushButton.setGeometry(QtCore.QRect(40, 180, 131, 23))
        self.pushButton.setObjectName("pushButton")
        self.groupBox = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox.setGeometry(QtCore.QRect(10, 20, 211, 61))
        self.groupBox.setObjectName("groupBox")
        self.comboBox = QtWidgets.QComboBox(self.groupBox)
        self.comboBox.setGeometry(QtCore.QRect(10, 20, 181, 22))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.graphicsView = QtWidgets.QGraphicsView(self.centralwidget)
        self.graphicsView.setGeometry(QtCore.QRect(260, 10, 451, 441))
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(30, 400, 91, 16))
        self.label_3.setOpenExternalLinks(True)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(30, 420, 91, 16))
        self.label_4.setOpenExternalLinks(True)
        self.label_4.setObjectName("label_4")
        self.graphicsView.setObjectName("graphicsView")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.graphicsView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff) ###
        self.graphicsView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff) ###
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.maze = []
        self.pushButton.clicked.connect(lambda: self.generate_labirint(self.spinBox.value(), self.spinBox_2.value()))
        self.pushButton_2.clicked.connect(lambda: self.save_labirint())

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Окно администратора"))
        self.groupBox_3.setTitle(_translate("MainWindow", "Создание лабиринта"))
        self.pushButton_2.setText(_translate("MainWindow", "Сохранить в файл"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Размерность"))
        self.label.setText(_translate("MainWindow", "По горизонтали"))
        self.label_2.setText(_translate("MainWindow", "По вертикали"))
        self.pushButton.setText(_translate("MainWindow", "Генерация лабиринта"))
        self.groupBox.setTitle(_translate("MainWindow", "Алгоритм лабиринта"))
        self.comboBox.setCurrentText(_translate("MainWindow", "алгоритм прима"))
        self.comboBox.setItemText(0, _translate("MainWindow", "алгоритм прима"))
        self.comboBox.setItemText(1, _translate("MainWindow", "алгоритм случайного поиска в глубину"))
        self.label_3.setText(f"<a href=\"{QtCore.QUrl.fromLocalFile('./html_pages/About_devs.html').toString()}\">О разработчиках</a>")
        self.label_4.setText(f"<a href=\"{QtCore.QUrl.fromLocalFile('./html_pages/About_program.html').toString()}\">О программе</a>")

    def print_labirint(self, maze, w, h):
        scene = QGraphicsScene(0,0,self.graphicsView.size().width(), self.graphicsView.size().height())
        square_widght = self.graphicsView.size().width()*0.99// w
        square_height = self.graphicsView.size().height()*0.99// h
        square_widght = square_height = min(square_widght, square_height)
        for i in range(0, h):
            for j in range(0, w):
                if (maze[i][j] == 0):
                    brush = QBrush(QColor(255, 128, 0))
                elif (maze[i][j] == 3):
                    brush = QBrush(QColor(0, 255, 255))
                elif (maze[i][j] == 5):
                    brush = QBrush(QColor(0, 255, 0))
                elif (maze[i][j] == 1):
                    brush = QBrush(QColor(255, 229, 204))
                square = QGraphicsRectItem(0, 0, square_widght, square_height)
                # brush = QBrush(Qt.red)
                square.setBrush(brush)
                square.setPos(i*square_widght, j*square_height)
                scene.addItem(square)
        self.graphicsView.setScene(scene)

    def get_alorithm(self):
        if self.comboBox.currentText() == "алгоритм прима":
            algo = "alorithm_prima"
        elif self.comboBox.currentText() == "алгоритм случайного поиска в глубину":
            algo = "alorithm_backtracking"
        else:
            algo = "other"
        return algo

    def generate_labirint(self, widght, height):
        if self.get_alorithm() == "alorithm_backtracking":
            backtracking = Backtracking(height, widght)
            maze = backtracking.createMaze()
            self.print_labirint(maze, widght, height)
            self.maze = maze
        elif self.get_alorithm() == "alorithm_prima":
            prim = PrimsMaze(height, widght)
            maze = prim.create_maze()
            self.print_labirint(maze, widght, height)
            self.maze = maze
            
    def save_labirint(self):
        if self.maze == []:
            msg = QMessageBox()
            msg.setWindowTitle("Ошибка")
            msg.setText("Сгенирируйте лабиринт, потом сохраняйте)")
            msg.setIcon(QMessageBox.Warning)
            msg.exec_()
        else:
            data = {}
            if not os.path.exists("./mazes"):
                os.mkdir("./mazes")
            dateTimeObj = datetime.now()
            timestampStr = dateTimeObj.strftime("%d_%b_%Y_%H_%M_%S")
            data.update({f'maze': f'{self.maze}'})
            data.update({f'algorithm': f'{str(self.get_alorithm())}'})
            file , check = QFileDialog.getSaveFileName(None, "Сохранение лабиринта",f"{self.get_alorithm()}_{timestampStr}.json", "Json Files (*.json)")
            if check:
                self.save_to_file(data, file)

    def save_to_file(self, data, path):
        json_object = json.dumps(data, indent=1)
        with open(path, "w+") as outfile:
            outfile.write(json_object)

    