import numpy as np
import matplotlib.pyplot as plt


class PrimsMaze:
    def __init__(self, width, height, show_maze=True):
        # self.size = (size // 2) * 2 + 1
        self.width = ((width-2)// 2) * 2 + 1
        self.height = ((height-2)// 2) * 2 + 1
        self.show_maze = show_maze
        self.walls_list = []
        self.grid = np.full((self.width, self.height), -50, dtype=int)
        for i in range((width-2)//2+1):
            for j in range((height-2)//2+1):
                self.grid[i*2, j*2] = -1
        self.maze = np.zeros((self.width, self.height), dtype=int)
        # print(self.grid)

    def is_valid(self, curr, dx, dy):
        x, y = curr
        if 0 <= x + dx < self.width and 0 <= y + dy < self.height:
            return True
        return False

    def add_neighbors(self, curr):
        nearby = [(-1, 0), (0, 1), (1, 0), (0, -1)]
        for dx, dy in nearby:
            if self.is_valid(curr, dx, dy):
                self.walls_list.append((curr[0]+dx, curr[1]+dy))

    def create_maze(self):
        start = (0, 0)
        start = ((start[0]//2)*2, (start[1]//2)*2)
        self.grid[start[0], start[1]] = 1
        #self.grid[0, ::2] = self.grid[-1, ::2] = 1
        #self.grid[::2, 0] = self.grid[::2, -1] = 1
        self.add_neighbors(start)
        while len(self.walls_list):
            ind = np.random.randint(0, len(self.walls_list))
            wall_x, wall_y = self.walls_list[ind]
            if self.is_valid((wall_x, wall_y), -1, 0) and self.is_valid((wall_x, wall_y), 1, 0):
                top = wall_x-1, wall_y
                bottom = wall_x+1, wall_y
                if (self.grid[top] == 1 and self.grid[bottom] == -1):
                    self.grid[wall_x, wall_y] = 1
                    self.grid[bottom] = 1
                    self.add_neighbors(bottom)
                elif (self.grid[top] == -1 and self.grid[bottom] == 1):
                    self.grid[wall_x, wall_y] = 1
                    self.grid[top] = 1
                    self.add_neighbors(top)
                self.walls_list.remove((wall_x, wall_y))
            if self.is_valid((wall_x, wall_y), 0, 1) and self.is_valid((wall_x, wall_y), 0, -1):
                left = wall_x, wall_y-1
                right = wall_x, wall_y+1
                if (self.grid[left] == 1 and self.grid[right] == -1):
                    self.grid[wall_x, wall_y] = 1
                    self.grid[right] = 1
                    self.add_neighbors(right)
                elif (self.grid[left] == -1 and self.grid[right] == 1):
                    self.grid[wall_x, wall_y] = 1
                    self.grid[left] = 1
                    self.add_neighbors(left)
                self.walls_list.remove((wall_x, wall_y))

            ''' 
            '''
        #     if self.show_maze:
        #         img = self.grid                 # Display maze while building
        #         plt.figure(1)
        #         plt.clf()
        #         plt.imshow(img)
        #         plt.title('Maze')
        #         plt.pause(0.005)
        #         #plt.pause(5)

        # plt.pause(5)

        for row in range(self.width):
            for col in range(self.height):
                if self.grid[row, col] == 1:
                    self.maze[row, col] = True

        self.maze[0, 0] = 3
        self.maze[self.width-1, self.height-1] = 5

        newrow = [0]*self.height
        self.maze = np.vstack([self.maze, newrow])
        self.maze = np.vstack([newrow, self.maze])
        self.width+=2
        mass = [[]*int(self.height+2)]*self.width
        for row in range(self.width):
            nl = []
            for col in range(self.height):
                if col == 0:
                    nl.append(0)
                    nl.append(self.maze[row][col])
                elif col == self.height-1:
                    nl.append(self.maze[row][col])
                    nl.append(0)
                else:
                    nl.append(self.maze[row][col])
            mass[row]=nl
        self.height+=2
                #     mass[row][1] = 0
                #     mass[row][self.width] = 0
                # else:
                #     mass[row][col] = self.maze[row][col]
        for row in range(self.width):
            for col in range(self.height):
                if mass[row][col] == 3:
                    mass[row][col-1] = 3
                    mass[row][col] = 1
                elif mass[row][col] == 5:
                    mass[row][col] = 1
                    mass[row][self.height-1] = 5
        #     # self.width+=1
        #     # self.maze[row] = self.maze[row]
        #     print(l)
                
        #         self.maze = np.append(self.maze, np.ones(1, dtype=int))

        # print(self.maze.dtype)
        return mass


# if __name__ == "__main__":                     # start <= (size, size)
#     obj = PrimsMaze(7, 9)
#     maze = obj.create_maze()
#     for i in maze:
#         print(i)
#     '''                                    
#     plt.figure(figsize=(10, 5))
#     plt.imshow(maze, interpolation='nearest')
#     plt.xticks([]), plt.yticks([])
#     plt.show()                              # Display final maze
#     '''