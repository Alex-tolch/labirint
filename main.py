from classes.Database import *
from classes.AuthWindow import Ui_AuthWindow
from classes.RegWindow1 import Ui_RegWindow
from classes.AdminMenu_2 import Ui_AdminMenu
from classes.labirint1 import Labirint_prima
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog
import sys

def open_reg_window():
    global open_reg
    open_reg = QtWidgets.QMainWindow()
    ui = Ui_RegWindow()
    ui.setupUi(open_reg)
    open_reg.show()

# def open_admin_window():
#     global open_admin
#     open_admin = QtWidgets.QMainWindow()
#     ui = Ui_AdminMenu()
#     ui.setupUi(open_admin)
#     open_admin.show()


def main():
    app = QtWidgets.QApplication(sys.argv)
    AuthWindow = QtWidgets.QMainWindow()
    ui = Ui_AuthWindow()
    ui.setupUi(AuthWindow)
    AuthWindow.show()

    # open registration window
    ui.pushButton_2.clicked.connect(open_reg_window)
    # logining
    ui.pushButton.clicked.connect(lambda: ui.loggin_user(ui.lineEdit.text(), ui.lineEdit_2.text(), AuthWindow))

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()